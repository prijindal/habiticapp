# Habitter
[![Build Status](https://travis-ci.org/prijindal/habiticapp.svg?branch=master)](https://travis-ci.org/prijindal/habiticapp) [![Coverage Status](https://coveralls.io/repos/github/prijindal/habiticapp/badge.svg?branch=master)](https://coveralls.io/github/prijindal/habiticapp?branch=master)


A Client for [Habitica Task Manager](https://habitica.com) built with [Flutter](https://flutter.io)

## Pre installed
Download apk from [here](https://github.com/prijindal/habiticapp/releases/download/auto-build/app-release.apk)

## Build it on your own

```
flutter run --release
```
